<h1 align="center">Team Page</h1>

<div align="center">
   Solution for a challenge from  <a href="http://devchallenges.io" target="_blank">Devchallenges.io</a>.
</div>

<div align="center">
  <h3>
    <a href="https://islambeg-frontend-projects.gitlab.io/vanilla/team-page">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/islambeg-frontend-projects/vanilla/team-page">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/hhmesazsqgKXrTkYkt0U">
      Challenge
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
  - [Built With](#built-with)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)

## Overview

This is a simple page of an imaginary team created with HTML and modern CSS features.

### Built With

- HTML
- CSS
- [Sass](https://sass-lang.com/)

## Acknowledgements

- [A modern CSS Reset](https://piccalil.li/blog/a-modern-css-reset/)
- [Favicon by Remix Design](https://remixicon.com/)

## Contact

- Email islambeg@proton.me
- Gitlab [@islambeg](https://gitlab.com/islambeg)
